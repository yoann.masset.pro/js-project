// The client socket connects to the server
var socket = io.connect();

var nicknameChoice = document.getElementById('nicknameChoice'); //The div to choose the nickname
var nicknameForm = document.getElementById('nicknameForm'); //The form to choose the nickname
var nickname = document.getElementById('nickname'); //The input to write the nickname
var characterRadioButtons = document.getElementsByName('character'); //The radio buttons to choose the character
var nicknameError = document.getElementById('nicknameError'); //The paragrph to display error

var appContent = document.getElementById('appContent'); //The div to display the content of the chat
var connectionInfo = document.getElementById('connectionInfo'); //The paragraph to display the connection information

// Function that return the value of the checked radio buttons
function checkRadioButtons(radioButtons) {
    for (var i = 0; i < radioButtons.length; i++) {
        if (radioButtons[i].checked == true) {
            return radioButtons[i].value;
        }
    }
}

var chatMessages = document.getElementById('chatMessages'); // The div to display the chat

// When a new user enter his nickname
nicknameForm.addEventListener('submit', e => {
    e.preventDefault();
    var character = checkRadioButtons(characterRadioButtons);
    
    var name = nickname.value.trim(); // Remove whitespace at the beggining end the end
    //Verify correct nickname
    if (name) { // If it's not void
        if(name.length <= 30){ // Nickname must have at most 30 characters
            socket.emit('new user', name, character, isGood => {
                if (isGood) {
                    socket.nickname = name;
                    console.log("Hello " + socket.nickname + " (character: " + character + ")");
                    nicknameError.textContent = "";
                    nicknameChoice.style.display = "none";
                    appContent.style.display = "block";
                    chatMessages.style.display = "none";
                    connectionInfo.textContent = "Welcome " + socket.nickname;
                } else {
                    nicknameError.textContent = name + " is already used ! Try again !";
                }
            });
        }
        else{
            nicknameError.textContent = "Nickname is too long ! 30 characters max ! Try again !";
        }
    } else {
        nicknameError.textContent = "Empty string is not a valid nickname ! Try again !";
    }
});

var connectedList = document.getElementById('connectedList'); //The div where connected users were been displayed

// When you want to display the connected users
socket.on('display users', usersList => {
    connectedList.textContent = "";
    for (var key in usersList) {
        if (usersList[key].chattingWith){
            var htmlText = usersList[key].nickname + " 🔴 (character: " + usersList[key].character + ")<br>";
        }else{
            var htmlText = usersList[key].nickname + " 🟢 (character: " + usersList[key].character + ")<br>";
        }
        connectedList.insertAdjacentHTML('beforeend', htmlText);
    }
});

// The variables of the background canvas
var groundCanvas = document.getElementById('ground');
var groundContext = groundCanvas.getContext('2d');

// The variables of the object canvas (where the character will be displayed)
var objectCanvas = document.getElementById('object');
var objectContext = objectCanvas.getContext('2d');

// To display the background in the canvas
var bg = new Image();
bg.src = "./src/Resources/newBackGround.png";
bg.onload = init_bg;

// Function to display the background in the canvas
function init_bg() {
    groundContext.drawImage(bg, 0, 0);
};

// Size of the canvas (the sizes of both canvas are equals)
var sizeCanvas = { 
    width: objectCanvas.width, 
    height: objectCanvas.height 
};

// The size of the character
const width_perso = 32;
const heigth_perso = 32;

// The directions that the characters can take
const FACING_DOWN = 1;
const FACING_UP = 0;
const FACING_LEFT = 2;
const FACING_RIGHT = 3;

// The variables to animate the character
const cycleLoop = [0, 1, 0, 2];
var currentLoopIndex = 0;
var frameCount = 0;

// Function to draw the fisrt time the character
function init_perso() {
    drawFrame(0, 0);
}

// Function to draw the character (To draw the character, we have a picture with 12 sprites in a different directions and different "speed")
// (img: the picture with the 12 sprites, frameX and frameY: the position of the needed character, posX and posY: the position where the character will be displayed)
function drawFrame(img, frameX, frameY, posX, posY) {
    objectContext.drawImage(img, frameX * width_perso, frameY * heigth_perso, width_perso, heigth_perso, posX - 16, posY - 16, width_perso * 2, heigth_perso * 2);
}

// The variables of the canvas where the user nickname will be displayed
var nameCanvas = document.getElementById("name");
var nameContext = nameCanvas.getContext("2d");
nameContext.font = "20px Arial";
nameContext.textAlign = "center";

// The deplacement of the current user
var deplacement = {
    up: false,
    down: false,
    left: false,
    right: false
}

// When pressing a key
document.addEventListener('keydown', event => {
    if (document.activeElement != message) { // If the focus is on the message input, the character can't move
        switch (event.key) {
            case "ArrowUp": case "z":
                deplacement.up = true;
                break;
            case "ArrowLeft": case "q":
                deplacement.left = true;
                break;
            case "ArrowDown": case "s":
                deplacement.down = true;
                break;
            case "ArrowRight": case "d":
                deplacement.right = true;
                break;
        }
    }
});

// When releasing a key
document.addEventListener('keyup', event => {
    if (document.activeElement != message) { // If the focus is on the message input, the character can't move
        switch (event.key) {
            case "ArrowUp": case "z":
                deplacement.up = false;
                break;
            case "ArrowLeft": case "q":
                deplacement.left = false;
                break;
            case "ArrowDown": case "s":
                deplacement.down = false;
                break;
            case "ArrowRight": case "d":
                deplacement.right = false;
                break;
        }
    }
});

// Send your deplacement to the server 30 times/seconds
setInterval(() => {
    socket.emit('deplacement', deplacement, sizeCanvas);
}, 1000 / 30); //30 FPS

// When you draw the character of all users
socket.on('positions', usersList => {

    // Clear the canvas to draw again each character
    objectContext.clearRect(0, 0, sizeCanvas.width, sizeCanvas.height);
    nameContext.clearRect(0,0,sizeCanvas.width, sizeCanvas.height);

    const FRAME_LIMIT = 10;
    for (var key in usersList) { // For each user in the list
        if (usersList[key].hasMoved) { // If the user has moved, we draw the character in a different "speed" each times
            
            // To animate the character
            frameCount++;
            if (frameCount >= FRAME_LIMIT) {
                frameCount = 0;
                currentLoopIndex++;
                if (currentLoopIndex >= cycleLoop.length) {
                    currentLoopIndex = 0;
                }
            }

            // Draw the character
            drawFrame(tabPerso[usersList[key].character], cycleLoop[currentLoopIndex], usersList[key].currentDirection, usersList[key].posX, usersList[key].posY);
        }
        else{ // The character doesn't move
            // Draw the character
            drawFrame(tabPerso[usersList[key].character], cycleLoop[0], usersList[key].currentDirection, usersList[key].posX, usersList[key].posY);
        }

        // Display the user nickname above the character
        if (usersList[key].chattingWith){ // If the user is chatting, we display it
            nameContext.fillStyle = "red";
            nameContext.fillText(usersList[key].nickname + "💬",usersList[key].posX+16, usersList[key].posY-10);
        }else{
            nameContext.fillStyle = "black";
            nameContext.fillText(usersList[key].nickname ,usersList[key].posX+16, usersList[key].posY-10);
        }
    }
});

// The table with the 3 pictures of the different character choice
var tabPerso = {};
for (var i = 1; i < 4; i++) {
    var perso = new Image();
    perso.src = "./src/Resources/perso" + i + ".png";
    perso.onload;
    tabPerso["perso" + i] = perso;
}

var generalMessages = document.getElementById('generalMessages'); // The paragraph with the messages of the genaral chat 

// Display a general message
socket.on('generalMessage', txt => {
    var message = txt + "<br>";
    generalMessages.insertAdjacentHTML('beforeend', message);

    // Auto scroll to the end of the general chat
    generalMessages.scrollTop = generalMessages.scrollHeight;
});

var askChat = document.getElementById('askChat'); // The div where we ask to the user if he want to chat
var askText = document.getElementById('askText'); // The text to ask the user
var askForm = document.getElementById('askForm'); // The form to get the answer of the user
var chatWith = null;

var buttonOk = document.getElementById('buttonOk'); // The ok button

// When 2 users are near enough to chat
socket.on('ask chat', name => {
    chatWith = name;

    // Display the frame to ask the user
    askChat.style.display = "block";
    askForm.style.display = "block";
    buttonOk.style.display = "none";
    askText.textContent = "Do you want to chat with " + name + " ?";
});

var buttonYes = document.getElementById('buttonYes'); // The yes button
var buttonNo = document.getElementById('buttonNo'); // The no button

// When the user press the yes button
buttonYes.addEventListener('click', e => {
    e.preventDefault();
    console.log("YES");

    // Remove the form to ask the user
    askForm.style.display = "none";
    askText.textContent = "Waiting for " + chatWith + " response ...";

    // Send that the user accept the chat request
    socket.emit("accept chat");
});

var chatMessages = document.getElementById('chatMessages'); // The div with the chat
var chatTitle = document.getElementById('chatTitle'); // the chat title
var allMessages = document.getElementById('allMessages'); //The paragraph where all messages were been displayed

// When the chat is accepted by the 2 users
socket.on('chat accepted', (user, name) => {

    // Remove the frame to ask the user
    askChat.style.display = "none";

    // Display the chat div
    chatMessages.style.display = "block";

    // Set the new chat title
    chatTitle.textContent = "";
    chatTitle.insertAdjacentHTML('beforeend', "<b>Chat with " + name + ":</b>");

    // Delete all old messages (if there were)
    allMessages.textContent = "";

    // Load the last 5 message if the 2 users have already chat together
    if (user.messagesWith[name]) { //If it's not void
        for (var i = 0; i < user.messagesWith[name].length; i++) {
            allMessages.insertAdjacentHTML('beforeend', user.messagesWith[name][i]);
        }
    }
});

// When the user doesn't want to chat
buttonNo.addEventListener('click', e => {
    e.preventDefault();
    console.log("NO");

    // Remove the frame to ask the user
    askChat.style.display = "none";

    // Send that the user cancel the chat request
    socket.emit('cancel chat');
});

// When the chat request was canceled
socket.on('chat canceled', name => {

    // Remove the form to ask the user
    askForm.style.display = "none";
    askText.textContent = name + " doesn't want to chat with you !";

    // Display the ok button
    buttonOk.style.display = "block";
});

// When the user click on ok
buttonOk.addEventListener('click', e => {
    e.preventDefault();

    // Remove element to the chat request
    buttonOk.style.display = "none";
    askChat.style.display = "none";
});

// When a 'stop ask' event occure
socket.on('stop ask', () => {
    // Remove the frame to ask the user
    askChat.style.display = "none";
});

var messageForm = document.getElementById('messageForm'); //The form to send the messge
var message = document.getElementById('message'); //The input that contains the message

// When you send a message
messageForm.addEventListener('submit', e => {
    e.preventDefault();
    if (message.value) { //If it's not a void message

        // Send the messsage to the server
        socket.emit('send message', message.value);
        message.value = '';
    }
});

// When a new message must be displayed
socket.on('new message', msg => {
    // Add the message to the end of all messages
    allMessages.insertAdjacentHTML('beforeend', msg);

    // Auto scroll to the end of messages
    allMessages.scrollTop = allMessages.scrollHeight;
});

var buttonStop = document.getElementById('buttonStop'); // The button to stop the chat

// When the user stops the chat with the button
buttonStop.addEventListener('click', e => {
    e.preventDefault();
    // Send that he wants to stop chatting
    socket.emit('stop chat');
});

// When the chat is stopped
socket.on('chat stopped', name => {

    // Remove the chat div
    chatMessages.style.display = "none";
    if(name !== socket.nickname){ // If it's not the current user that emit this event

        // Display that the chat stops
        askChat.style.display = "block";
        buttonOk.style.display = "block";
        askForm.style.display = "none";
        askText.textContent = name + " stops chatting with you !";
    }
});

var disconnectForm = document.getElementById('disconnection'); // The form to disconnect a user

// When the user disconnects with the button
disconnectForm.addEventListener('submit', e => {
    e.preventDefault();

    // Send to the server that the user is disconnecting
    socket.emit('disconnection');

    // Remove all the app and display again the frame to choose a nickname
    appContent.style.display = "none";
    nicknameChoice.style.display = "block";
});
