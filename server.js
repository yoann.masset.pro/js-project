// Importing modules
const express = require('express');
const http = require('http');
const { Server } = require("socket.io");

// Server creation
const app = express();
const server = http.createServer(app);
const io = new Server(server);

// The port on which the server will run 
const port = 3000;

// List of all connected users with their information
var usersList = {};

// The folder used for source files
app.use('/src', express.static(__dirname + '/src'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

// When a 'connection' event append 
io.on('connection', socket => {

    // When a new user try to connect
    socket.on('new user', (nickname, character, isGood) => {
        if (!(nickname in usersList)) { // if the nickname doesn't exist in the list
            isGood(true); // Return isGood as a function

            var txtConnect = "Hello " + nickname + " (character: " + character + ")";
            console.log(txtConnect);

            // Create the user and his information and add him to the list of connected users
            socket.nickname = nickname;
            usersList[socket.nickname] = {
                id: socket.id, // The id
                nickname: socket.nickname, // the nickname
                character: character, // the character choosen by the user
                posX: 400, // the position
                posY: 300, // the position
                nearForChat: null, // the near user that can chat with
                chattingWith: null, // the user that chat with
                acceptChat: false, // if the user accept the chat or not
                messagesWith: {}, // 5 last messages with all connected users (if the users speak at least once)
                deplacement: {up: false, down: false, left: false, right: false}, // the deplacement of the user
                currentDirection: 1, // The direction where the character goes
                hasMoved: false // If the character has moved
            };

            // Send a 'generalMessage' event to all sockets
            io.emit('generalMessage', txtConnect);

            // Send event to all sockets to display the connected users
            io.emit('display users', usersList);
        } else {
            isGood(false);
        }
    });

    const SPEED = 5; // The speed of the character
    const DISTANCE1 = 40; // The distance between 2 characters to enter in chat
    const DISTANCE2 = 60; // The distance between 2 characters to stop a chat

    // When a 'deplacement' event arrive
    socket.on('deplacement', (move, size) => {
        if (socket.nickname) { // If the socket nickname is not undefined (it's undefined if the user doesn't enter a nickname)
            // Get the user in the list who send this event 
            var user = usersList[socket.nickname];
            user.deplacement = move; // Set the deplacement of the user

            // Check if the character move or not
            if (move.up) {
                user.posY -= SPEED;
                if (user.posY < 0) user.posY = 0;
            }
            if (move.left) {
                user.posX -= SPEED;
                if (user.posX < 0) user.posX = 0;
            }
            if (move.down) {
                user.posY += SPEED;
                if (user.posY > size.height) user.posY = size.height;
            }
            if (move.right) {
                user.posX += SPEED;
                if (user.posX > size.width) user.posX = size.width;
            }

            checkDirection(user);

            // Check the distance between the user who send this event and the other
            checkDistanceUsers(user);
        }
    });

    // The directions that the characters can take
    const FACING_DOWN = 1;
    const FACING_UP = 0;
    const FACING_LEFT = 2;
    const FACING_RIGHT = 3;

    // Function to check the direction of the character
    function checkDirection(user){
        if (user.deplacement.right) {
            user.currentDirection = FACING_RIGHT;
            user.hasMoved = true;
        } else if (user.deplacement.down) {
            user.currentDirection = FACING_DOWN;
            user.hasMoved = true;
        } else if (user.deplacement.left) {
            user.currentDirection = FACING_LEFT;
            user.hasMoved = true;
        } else if (user.deplacement.up) {
            user.currentDirection = FACING_UP;
            user.hasMoved = true;
        } else {
            user.hasMoved = false;
        }
    }

    // Function to check the distance between users
    function checkDistanceUsers(user) {

        // look all connected users
        for (var key in usersList) {
            var otherUser = usersList[key];

            if (user.nickname !== otherUser.nickname) { // check if we don't calculate the distance between the same user
                var dist = Math.sqrt(Math.pow(user.posX - otherUser.posX, 2) + Math.pow(user.posY - otherUser.posY, 2));

                if (dist < DISTANCE1 && user.nearForChat == null && otherUser.nearForChat == null) { // 2 near users that don't already chat with another one
                    // Set the near user
                    otherUser.nearForChat = user.nickname;
                    user.nearForChat = otherUser.nickname;

                    // Send 'ask chat' event only to the concerned users to ask them if they want to chat
                    io.to(user.id).emit('ask chat', otherUser.nickname);
                    io.to(otherUser.id).emit('ask chat', user.nickname);
                }
                else if (dist > DISTANCE2 && user.nearForChat == otherUser.nickname && otherUser.nearForChat == user.nickname) { // If the user move away
                    if (user.chattingWith === otherUser.nickname && otherUser.chattingWith === user.nickname) { // If the users are chatting together
                        stopChat(user, otherUser);
                    }
                    else {
                        user.nearForChat = null;
                        user.acceptChat = false;
                        otherUser.nearForChat = null;
                        otherUser.acceptChat = false;

                        // Send 'stop ask' event to the users (remove the ask window)
                        io.to(user.id).emit('stop ask');
                        io.to(otherUser.id).emit('stop ask');
                    }
                }
            }
        }
    }

    // Function to stop a chat between 2 users
    function stopChat(user, otherUser) {
        user.chattingWith = null;
        user.acceptChat = false;
        otherUser.chattingWith = null;
        otherUser.acceptChat = false;

        io.emit('display users', usersList);

        // Send 'chat stopped' event only to the concerned users (user.nickname is the user who stopped the chat)
        io.to(user.id).emit('chat stopped', user.nickname);
        io.to(otherUser.id).emit('chat stopped', user.nickname);
    }

    // On a 'stop chat' event (when 1 user stops the chat)
    socket.on('stop chat', () => {
        var user = usersList[socket.nickname];
        var otherUser = usersList[user.chattingWith];
        stopChat(user, otherUser);
    });

    // When a user cancels the chat request
    socket.on('cancel chat', () => {
        var user = usersList[socket.nickname];
        user.acceptChat = false;
        var otherUser = usersList[user.nearForChat];
        otherUser.acceptChat = false;

        // Send the event to the other user to notice that the user cancelled the chat request
        io.to(otherUser.id).emit('chat canceled', socket.nickname);
    });

    // When a user accept the chat request
    socket.on('accept chat', () => {
        var user = usersList[socket.nickname];
        user.acceptChat = true;
        var otherUser = usersList[user.nearForChat];

        if (otherUser.acceptChat) { // If the 2 users have accepted the chat request 
            user.chattingWith = otherUser.nickname;
            otherUser.chattingWith = user.nickname;

            io.emit('display users', usersList);

            // Send Send 'chat stopped' event only to the concerned users (user.nickname is the user who stopped the chat)
            io.to(user.id).emit('chat accepted', user, otherUser.nickname);
            io.to(otherUser.id).emit('chat accepted', otherUser, user.nickname);
        }
    });

    // Function to add the message to the list of message with a user
    function addMessageUser(user, name, msg) {
        if (!user.messagesWith[name]) {// If it's the fisrt message of the list
            user.messagesWith[name] = [msg];
        }
        else {
            user.messagesWith[name].push(msg); // Push the message at the end of the list
            while (user.messagesWith[name].length > 5) { // Delete the fisrt one to keep just the 5 last messages
                user.messagesWith[name].shift();
            }
        }
    }

    // When you send a message, just you and the concerned user will receive it
    socket.on('send message', msg => {
        // Get the time when the message have been sent
        const now = new Date();
        var time = now.toLocaleTimeString('en-us', {hour: '2-digit', minute: '2-digit'});

        var user = usersList[socket.nickname];
        var otherUser = usersList[user.chattingWith];
        var message = time + " <b>" + user.nickname + "</b>: " + msg + "<br>";

        // Send the message to the 2 concerned users
        io.to(user.id).emit('new message', message);
        io.to(otherUser.id).emit('new message', message);

        // Add the message to the list of both users
        addMessageUser(user, otherUser.nickname, message);
        addMessageUser(otherUser, user.nickname, message);
    });

    // Send the position (and the other information) of all users to all socket 30 times/seconds
    setInterval(() => {
        io.emit('positions', usersList);
    }, 1000 / 30); //30 FPS

    // Function when a user disconnect
    function disconnectionFct() {
        if (socket.nickname) { // If the nickname is not null (when you are in the web page but you don't enter your nickname, your socket nickname is null)
            var txtDisconnect = "Good bye " + socket.nickname + "💨";
            console.log(txtDisconnect);

            // Check if someone is near the disconnecting user
            var name = usersList[socket.nickname].nearForChat;
            if (name) {
                var otherUser = usersList[name];

                // Set that the other user is ""alone""
                otherUser.nearForChat = null;
                otherUser.chattingWith = null;
                otherUser.acceptChat = false;

                // Send to the near user that chat stopped
                io.to(otherUser.id).emit('chat stopped', socket.nickname);
            }

            // Delete all the messages with the disconnecting user
            for (var key in usersList[socket.nickname].messagesWith) {
                var otherUser = usersList[key];
                delete otherUser.messagesWith[socket.nickname];
            }

            // Delete the disconnecting user from the user list
            delete usersList[socket.nickname];
            socket.nickname = null;

            // Send events to notify that someone disconnects
            io.emit('generalMessage', txtDisconnect);
            io.emit('display users', usersList); //Display connected users (other clients won't see the disconnected user)
        }
    }

    // On a 'disconnection' event (the event uccured when you click on the disconnect button)
    socket.on('disconnection', () => {
        disconnectionFct();
    });

    // When someone disconnects without the disconnect button (refresh or close the window)
    // It's the event by default when disconnecting
    socket.on('disconnect', () => {
        disconnectionFct();
    });
});

// Set the server listening on the port previously defined
server.listen(port, () => {
    console.log('listening on :' + port);
});
