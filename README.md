# JS Project - MASSET - MASSOT


**Lancement de l'appli :**
1. Télécharger tous les fichiers (index.html, server.js, package.json, src/game.js, src/style.css) et les mettre dans un dossier (qu'on appelera "racine")
2. Avoir téléchargé Node.js
3. Dans le dossier "racine", executer la commande : ```npm install --save express socket.io```
4. Lancer le serveur avec la commande : ```node server.js```
5. Ouvrir un navigateur et entrer l'url : http://localhost:3000/
6. Et voilà

***

## Précisions

* Lors qu'un utilisateur se déconnecte, même s'il se reconnecte avec le même nom, l'historique des messages est perdu
* Nous n'avons pas eu le temps d'optimiser le code et donc les échanges entre les clients et le server. C'est pour cela qu'il faut généralement éviter de se connecter à plus de 4 utilisateurs sur le serveur, car celui-ci aura du mal à suivre. Avec une meilleure connaissance du web et une meilleure organisation de notre temps, nous aurions pu résoudre ou au moins minimiser ce problème.  

***

Texture : https://pokemonworkshop.fr/forum/index.php?topic=3836.0
